FROM python:3.9-slim

COPY requirements.txt
RUN pip install -r requirements.txt

COPY clinic/ ./

COPY docker-entrypoint.sh ./
ENTRYPOINT ["./docker-entrypoint.sh"]

CMD [ "gunicorn", "clinic.wsgi"]
